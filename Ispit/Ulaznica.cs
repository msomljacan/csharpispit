﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ispit
{
    sealed class Ulaznica
    {
        public Ulaznica(string ime, string prezime, int redniBroj)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.redniBroj = redniBroj;
            vrijeme = new DateTime(); 
        }

        public override string ToString()
        {
            return redniBroj + ". " + redniBroj + " - " + ime + " " + prezime + " - " + vrijeme;
        }

        public string ime { get; set; }
        public string prezime { get; set; }
        public int redniBroj { get; set; }
        DateTime vrijeme { get; set; }
    }
}
