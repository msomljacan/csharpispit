﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ispit
{
    class Vrtuljak : Atrakcija
    {
        public Vrtuljak(string tip, decimal cijena) : base(tip, cijena)
        {
        }

        public override void KupiUlaznicu(string ime, string prezime)
        {
            try
            {
                if (ulaznice.Count == 0)
                {
                    ulaznice.Add(new Ulaznica(ime, prezime, ulaznice.Count + 50));
                }
                else if (ulaznice.Count < 500)
                {
                    ulaznice.Add(new Ulaznica(ime, prezime, ulaznice.Count + 1));
                }
                else
                {
                    throw new Exception("Maksimalan broj posjetitelja");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
