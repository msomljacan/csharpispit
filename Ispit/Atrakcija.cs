﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ispit
{
    abstract class Atrakcija : IAtrakcija
    {
        public Atrakcija(string tip, decimal cijena)
        {
            this.tip = tip;
            this.cijena = cijena;
            ulaznice = new List<Ulaznica>();
        }

        public abstract void KupiUlaznicu(string ime, string prezime);

        public void PrikaziUlaznice()
        {
            foreach (var ulaznica in ulaznice)
            {
                Console.WriteLine(ulaznica.ToString());
            }
        }

        public void PromijeniCijenu(Ulaznica ulaznica, decimal novaCijena)
        {
            try
            {
                if(novaCijena < cijena)
                {
                    cijena = novaCijena;
                }
                else
                {
                    throw new Exception("Zabranjeno povecanje cijene!");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private string tip { get; set; }
        private decimal cijena { get; set; }
        protected List<Ulaznica> ulaznice { get; set; }
    }
}
