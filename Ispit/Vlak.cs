﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ispit
{
    class Vlak : Atrakcija
    {
        public Vlak(string tip, decimal cijena) : base(tip, cijena) { }
        public override void KupiUlaznicu(string ime, string prezime)
        {
            try
            {
                if (ulaznice.Count == 1000)
                {
                    throw new Exception("Rasprodano");
                }
                else
                {
                    ulaznice.Add(new Ulaznica(ime, prezime, ulaznice.Count + 1));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
